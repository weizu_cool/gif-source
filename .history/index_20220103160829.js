const http = require('http')
const fs = require('fs')

const server = http.createServer(function (req, res) {
  // 加载首页
  fs.stat('index.html', function (error, data) {
    if (error) {
      response.writeHead(404)
      response.end('404 Not Found.')
    } else {
      response.writeHead(200)
      fs.createReadStream(filepath).pipe(res)
    }
  })
})

server.listen(3001, 'localhost', () => {
  console.log(`Server running at http://localhost:3001/`)
})
