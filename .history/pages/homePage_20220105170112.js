const fs = require('fs')

module.exports = {
  home: function (req, res) {
    // 加载首页
    var filePath = 'index.html'
    fs.stat(filePath, function (error, data) {
      res.writeHead(200)
      fs.createReadStream(filePath).pipe(res)
    })
  },
}
