const fs = require('fs')

module.exports = {
  home: function (req, res) {
    // 加载404
    var filePath = '404.html'
    fs.stat(filePath, function (error, data) {
      if (error) {
        res.writeHead(404)
        res.end('404.html Not Found.')
      } else {
        res.writeHead(200)
        fs.createReadStream(filePath).pipe(res)
      }
    })
  },
}
