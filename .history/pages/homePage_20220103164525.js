const fs = require('fs')

module.exports = {
  home: function () {
    // 加载首页
    var filePath = 'index.html'
    fs.stat(filePath, function (error, data) {
      if (error) {
        res.writeHead(404)
        res.end('404 index.html Not Found.')
      } else {
        res.writeHead(200)
        fs.createReadStream(filePath).pipe(res)
      }
    })
  },
}
