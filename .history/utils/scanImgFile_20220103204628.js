// 扫描目录下的png图片文件
const fs = require('fs')

var maps = {}

module.exports = {
  scan: function () {
    var path = __dirname.substring(0, __dirname.length - 6)
    fs.readdir(path, function (error, files) {
      for (var i = 0; i < files.length; i++) {
        var fileName = files[i].toString()
        if (fileName.length > 4) {
          if (
            fileName.endsWith('.png') ||
            fileName.endsWith('.jpg') ||
            fileName.endsWith('.gif')
          ) {
            maps[fileName] = 1
          }
        }
      }
    })
    return Object.keys(global.maps)
  },
}
