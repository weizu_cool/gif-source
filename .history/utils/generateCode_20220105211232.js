// 生成HTML文件
const fs = require('fs')
const { listeners } = require('process')

function getFileName(filename) {
  return filename.substring(0, filename.length - 4)
}

module.exports = {
  process: function (datas, url) {
    console.log('url: ', url)
    var res = fs.readFileSync(__dirname + '/forePage.txt', 'utf8')
    var jsonDatas = {
      day: [],
      week: [],
      month: [],
      year: [],
    }

    Object.keys(datas).map((item) => {
      console.log('item: ', item)
      var keys = datas[item]
      for (var i = 0; i < keys.length; i++) {
        jsonDatas[item].push({ src: keys[i], title: getFileName(keys[i]) })
      }
    })

    var str = 'const datas = ' + JSON.stringify(jsonDatas)

    res += str

    res += fs.readFileSync(__dirname + '/afterPage.txt', 'utf8')
    // 写文件
    fs.writeFileSync('index.html', res)
  },
}
