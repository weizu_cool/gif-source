// 生成HTML文件
const fs = require('fs')

function getFileName(filename) {
  return filename.substring(0, filename.length - 4)
}

module.exports = {
  process: function (keys) {
    var res = fs.readFileSync(__dirname + '/forePage.txt', 'utf8')
    for (var i = 0; i < keys.length; i++) {
      console.log('keys[i]:' + keys[i])
      res +=
        '<div class="image"><div class="image-view"><img src="public/' +
        keys[i] +
        '" /></div><p class="image-title">' +
        getFileName(keys[i]) +
        '</p><span class="copy-btn copy-btn-1">open</span><span class="copy-btn copy-btn-2">copy link</span>' +
        '<span class="copy-btn copy-btn-3">copy html</span></div>'
    }
    res += fs.readFileSync(__dirname + '/afterPage.txt', 'utf8')
    // 写文件
    fs.writeFileSync('index.html', res)
  },
}
