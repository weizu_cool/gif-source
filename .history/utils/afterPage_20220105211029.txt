var totalElements = {
            day: document.getElementById('day'),
            week: document.getElementById('week'),
            month: document.getElementById('month'),
            year: document.getElementById('year'),
        }
        var totalHTML = {
            day: undefined,
            week: undefined,
            month: undefined,
            year: undefined
        }
      window.onload = function () {

        // 动态渲染HTML页面
        Object.keys(datas).forEach(key => {
            if(totalHTML[key] == undefined || totalHTML[key] == null || totalHTML[key].length == 0){
                totalHTML[key] = "";
            datas.day.map(item => {
                totalHTML[key] += '<div class="image"><div class="image-view"><img src="' +  item.src +
                '" /></div><p class="image-title">' + item.title +
                '</p><span class="copy-btn copy-btn-1">open</span><span class="copy-btn copy-btn-2">copy link</span>' +
                '<span class="copy-btn copy-btn-3">copy html</span></div>'
            })
            totalElements[key].innerHTML = totalHTML[key];
        }
        });

        // 响应用户事件
        const baseURL = window.location.hostname
        var selected = document.getElementById('show')
        var links = window.location.href.split("?");
        if(links.length != 1){
            var val = links[1].split("=")[1];
            selected.value = val;
        }
        selected.onclick = function(e){
            var value = this.value;
            if(links.length == 1 ){
                window.location.href = '?show=' + value;
            } else{
                var link_value = links[1].split("=")[1];
                if(link_value != value) {
                    window.location.href = '?show=' + value;
                }
            }
        }

        
        var imgs = document.getElementsByClassName('image')
        var open = document.getElementsByClassName('copy-btn-1')
        for (var i = 0; i < open.length; i++) {
            open[i].onclick = function () {
            var image = this.parentNode.getElementsByTagName('img')[0]
            var url = window.location.href + image.getAttribute('src')
            window.open(url)
          }
        }
        var btns = document.getElementsByClassName('copy-btn-2')
        for (var i = 0; i < btns.length; i++) {
          btns[i].onclick = function () {
            var image = this.parentNode.getElementsByTagName('img')[0]
            var url = window.location.href + image.getAttribute('src')
            var input = document.createElement('input')
            input.setAttribute('readonly', 'readonly')
            input.setAttribute('value', url)
            document.body.appendChild(input)
            input.select()
            var res = document.execCommand('copy')
              ? '复制链接成功！'
              : '复制链接失败！'
            document.body.removeChild(input)
          }
        }
        var html = document.getElementsByClassName('copy-btn-3')
        for (var i = 0; i < html.length; i++) {
            html[i].onclick = function () {
            var image = this.parentNode.getElementsByTagName('img')[0]
            var url = window.location.href + image.getAttribute('src')
            var img = "<img src='" + url + "'/>";
            var input = document.createElement('input')
            input.setAttribute('readonly', 'readonly')
            input.setAttribute('value', img)
            document.body.appendChild(input)
            input.select()
            var res = document.execCommand('copy')
              ? '复制链接成功！'
              : '复制链接失败！'
            document.body.removeChild(input)
          }
        }
      }
    </script>
  </body>
</html>
