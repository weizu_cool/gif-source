// 扫描目录下的png图片文件
const fs = require('fs')

function getType(link) {
  //文件类型
  return link.substring(link.length - 4, link.length)
}

var maps = {}

module.exports = {
  scan: function (req, res) {
    var path = __dirname.substring(0, __dirname.length - 6)
    fs.readdir(path, function (error, files) {
      for (var i = 0; i < files.length; i++) {
        var fileName = files[i].toString()
        if (fileName.length > 4) {
          if (
            fileName.endsWith('.png') ||
            fileName.endsWith('.jpg') ||
            fileName.endsWith('.gif')
          ) {
            maps[fileName] = 1
          }
        }
      }
    })
    return maps
  },
}
