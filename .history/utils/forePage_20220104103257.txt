<!DOCTYPE html>
<html>
  <head>
    <title>我的博客图床</title>
    <meta charset="utf-8"></head>
    <style>
      *,
      div,
      p {
        border: 0;
        margin: 0;
        padding: 0;
      }
      .Container {
        width: 90vw;
        margin: 10px auto 0 auto;
        box-shadow: 5px;
      }
      .image-view {
        border: 3px dashed rgb(187, 185, 185);
      }
      .image {
        height: 200px;
        position: relative;
        margin: 10px;
        float: left;
      }
      .image-view img {
        width: 100%;
      }
      .image-title {
        width: 100%;
        text-align: center;
        font-size: 1rem;
        color: rgb(59, 142, 145);
      }
      .copy-btn {
        position: absolute;
        right: 3px;
        top: 3px;
        display: block;
        background: gray;
        font-size: 0.8rem;
        padding: 3px;
        color: white;
        cursor: pointer;
        border-radius: 5px;
        box-shadow: 3px 3px 2px rgb(172, 172, 172);
      }
      .copy-btn:hover {
        background: rgb(53, 52, 52);
      }
    </style>
  </head>
  <body>
    <div class="Container">