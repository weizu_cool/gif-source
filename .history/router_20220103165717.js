module.exports = {
  processImage: function () {
    //文件类型
    var type = req.url.substring(req.url.length - 4, req.url.length)
    //资源路径
    var fileName = __dirname + req.url
    //加载需要显示的图片资源
    if (type == '.jpg' || type == '.png') {
      res.writeHead(200, { 'Content-Type': 'text/' + type })
      res.end(fs.readFileSync(fileName))
    }
  },
}
