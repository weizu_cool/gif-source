const http = require('http')
const fs = require('fs')

const server = http.createServer(function (req, res) {
  res.writeHead(200, { 'Content-Type': "text/html;charset='utf-8'" })
  // 加载首页
  fs.readFile('index.html', 'utf-8', function (error, data) {
    res.end(data)
    console.log(error)
  })
})

server.listen(3001, 'localhost', () => {
  console.log(`Server running at http://localhost:3001/`)
})
