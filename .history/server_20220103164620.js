var http = require('http')
var file = require('./components/loadFile')
var homePage = require('./pages/homePage')

function start() {
  const server = http.createServer(function (req, res) {
    //文件类型
    var type = req.url.substring(req.url.length - 4, req.url.length)
    //资源路径
    var fileName = req.url
    //加载需要显示的图片资源
    if (type == '.jpg' || type == '.png') {
      res.writeHead(200, { 'Content-Type': 'text/' + type })
      res.end(file.readFile(fileName))
    }

    // 加载首页
    homePage.home(res)
  })

  server.listen(3001, 'localhost', () => {
    console.log(`Server running at http://localhost:3001/`)
  })
}

exports.start = start
