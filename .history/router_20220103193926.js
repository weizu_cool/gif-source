const fs = require('fs')
const homePage = require('./pages/homePage')
const examplePages = require('./pages/examplePages')
const struils = require('./utils/stringUtil')

const routes = {
  login: { page: examplePages.login, type: 'text/plain' },
  hello: { page: examplePages.hello, type: 'text/plain' },
  index: { page: homePage.home, type: 'text/html' },
  404: { page: homePage.home, type: 'text/html' },
}

function getType(req, res) {
  //文件类型
  return req.url.substring(req.url.length - 4, req.url.length)
}

/**
 * 处理加载图片
 */
function processImage(req, res) {
  var type = getType(req, res)
  //资源路径
  var fileName = __dirname + req.url
  //加载需要显示的图片资源
  if (type == '.jpg' || type == '.png') {
    fs.readFile(fileName, 'binary', function (err, file) {
      if (err) {
        console.error(err)
      } else {
        res.writeHead(200, { 'Content-Type': 'image/jpeg' })
        res.write(file, 'binary')
        res.end()
      }
    })
  }
}

/**
 * 处理其余链接
 */
function processURL(req, res) {
  var url = ''
  if (struils.trim(req.url) == '/') url = ''
  else url = struils.trim(req.url.substring(1))

  // 首页
  if (url.length == 0 || url == '' || url == 'index.html' || url == 'index') {
    routes.index.page(req, res)
  } else if (url == 'login') {
    res.writeHead(200, { 'Content-Type': routes.login.type })
    res.end(routes.login.page())
  } else if (url == 'hello') {
    res.writeHead(200, { 'Content-Type': routes.hello.type })
    res.end(routes.hello.page())
  }
}

module.exports = {
  /**
   * 注册路由信息
   * @param {*} maps  link: function
   */
  registerRoute: function (req, res, maps) {
    var url = req.url
  },

  /**
   * 处理路由请求
   */
  processRoute: function (req, res) {
    // 其余链接
    processURL(req, res)
    // 处理图片
    processImage(req, res)
  },
}
