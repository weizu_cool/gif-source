const http = require('http')

const server = http.createServer(function (req, res) {
  res.writeHead(200, { 'Content-Type': 'text/plain' })
  res.end('hello')
})

server.listen(3001, 'localhost', () => {
  console.log(`Server running at http://localhost:3001/`)
})
