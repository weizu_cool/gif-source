const fs = require('fs')
const homePage = require('./pages/homePage')
const notfound = require('./pages/notfound')
const examplePages = require('./pages/examplePages')
const struils = require('./utils/stringUtil')
var urlencode = require('urlencode')

const routes = {
  login: { page: examplePages.login, type: 'text/plain' },
  hello: { page: examplePages.hello, type: 'text/plain' },
  index: { page: homePage.home, type: 'text/html' },
  errorPage: { page: notfound.notfound, type: 'text/html' },
}

function getType(req, res) {
  //文件类型
  return req.url.substring(req.url.length - 4, req.url.length)
}

function loadImageFile(fileName, res) {
  fs.readFile(fileName, 'binary', function (err, file) {
    if (err) {
      res.writeHead(200, { 'Content-Type': 'text/plain' })
      res.end('Resource not exist.')
    } else {
      res.writeHead(200, { 'Content-Type': 'image/jpeg' })
      res.write(file, 'binary')
      res.end()
    }
  })
}

/**
 * 处理加载图片
 */
function processImage(req, res) {
  var type = getType(req, res)
  //资源路径
  var fileName = urlencode.decode(__dirname + req.url)
  //加载需要显示的图片资源
  if (type == '.jpg' || type == '.png' || type == '.gif' || type == '.ico') {
    loadImageFile(fileName, res)
    return true
  }
  return false // 放行非图片资源
}

/**
 * 处理其余链接
 */
function processURL(req, res) {
  var url = req.url
  if (struils.trim(url) == '/') url = ''
  else url = struils.trim(url.substring(1))
  var pars = url.split('?')
  if (pars.length != 1) {
    url = pars[0]
  }
  console.log(url)

  // 首页
  if (url.length == 0 || url == '' || url == 'index.html' || url == 'index') {
    routes.index.page(req, res)
  } else if (url == 'login') {
    res.writeHead(200, { 'Content-Type': routes.login.type })
    res.end(routes.login.page())
  } else if (url == 'hello') {
    res.writeHead(200, { 'Content-Type': routes.hello.type })
    res.end(routes.hello.page())
  } else {
    // 404
    routes.errorPage.page(req, res)
  }
}

module.exports = {
  /**
   * 注册路由信息
   * @param {*} maps  link: function
   */
  registerRoute: function (req, res, maps) {
    var url = req.url
  },

  /**
   * 处理路由请求
   */
  processRoute: function (req, res) {
    // 处理图片
    var flag = processImage(req, res)
    if (!flag) {
      // 其余链接
      processURL(req, res)
    }
  },
}
