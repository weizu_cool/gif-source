var http = require('http')
const router = require('./router')
const imgscan = require('./utils/scanImgFile')
const generateCode = require('./utils/generateCode')

global.lastKeys = undefined

function start() {
  const server = http.createServer(function (req, res) {
    // 显示内容
    router.processRoute(req, res)

    // 扫描文件
    var keys = imgscan.scan(req, res)
    if (global.lastKeys == undefined) {
      global.lastKeys = keys
      generateCode.process(keys)
    }
  })

  server.listen(3001, 'localhost', () => {
    console.log(`Server running at http://localhost:3001/`)
  })
}

exports.start = start
