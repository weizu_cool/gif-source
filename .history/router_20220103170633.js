const fs = require('fs')
const examplePages = require('./pages/examplePages')

const routes = [{ login: examplePages.login }, { hello: examplePages.hello }]

function processImage(req, res) {
  //文件类型
  var type = req.url.substring(req.url.length - 4, req.url.length)
  //资源路径
  var fileName = __dirname + req.url
  //加载需要显示的图片资源
  if (type == '.jpg' || type == '.png') {
    res.writeHead(200, { 'Content-Type': 'text/' + type })
    res.end(fs.readFileSync(fileName))
  }
}

module.exports = {
  /**
   * 注册路由信息
   * @param {*} maps  link: function
   */
  register: function (req, res, maps) {
    var url = req.url
    console.log(url)
  },

  /**
   * 处理路由请求
   */
  processRoute: function (req, res) {
    var url = req.url
    console.log(url)
  },
}
