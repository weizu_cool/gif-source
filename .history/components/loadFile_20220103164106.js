const fs = require('fs')

module.exports = {
  readFile: function (fileName) {
    return fs.readFileSync(fileName, 'utf-8')
  },
}
