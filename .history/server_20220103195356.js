var http = require('http')
const router = require('./router')
const imgscan = require('./utils/scanImgFile')

function start() {
  const server = http.createServer(function (req, res) {
    // 显示内容
    router.processRoute(req, res)

    // 扫描文件
    imgscan.scan(req, res)
  })

  server.listen(3001, 'localhost', () => {
    console.log(`Server running at http://localhost:3001/`)
  })
}

exports.start = start
