const fs = require('fs')
const homePage = require('./pages/homePage')
const examplePages = require('./pages/examplePages')
const struils = require('./utils/stringUtil')

const routes = {
  login: { page: examplePages.login, type: 'text/plain' },
  hello: { page: examplePages.hello, type: 'text/plain' },
  index: { page: homePage.home, type: 'text/html' },
}

/**
 * 处理加载图片
 */
function processImage(req, res) {
  //文件类型
  var type = req.url.substring(req.url.length - 4, req.url.length)
  //资源路径
  var fileName = __dirname + req.url
  //加载需要显示的图片资源
  if (type == '.jpg' || type == '.png') {
    console.log(fileName)
    fs.readFile(fileName, 'binary', function (err, file) {
      if (err) {
        console.error(err)
      } else {
        res.writeHead(200, { 'Content-Type': 'image/jpeg' })
        res.write(file, 'binary')
        res.end()
      }
    })
  }
}

/**
 * 检查url是否注册，没有注册返回null
 *  url
 */
function registed(url) {
  Object.keys(routes).forEach(function (key) {
    if (url == key) {
      return true
    }
  })
  return false
}

/**
 * 处理其余链接
 */
function processURL(req, res) {
  var url = req.url
  console.log(url)
  var isRegist = registed(url.substring(1))
  // 首页
  if (
    struils.trim(url).length == 0 ||
    struils.trim(url) == '/' ||
    struils.trim(url) == '/index.html' ||
    struils.trim(url) == '/index'
  ) {
    routes.index.page(req, res)
  } else if (isRegist) {
  } else {
    // 返回404
    console.log(404)
  }
}

module.exports = {
  /**
   * 注册路由信息
   * @param {*} maps  link: function
   */
  registerRoute: function (req, res, maps) {
    var url = req.url
  },

  /**
   * 处理路由请求
   */
  processRoute: function (req, res) {
    // 处理图片
    processImage(req, res)
    // 其余链接
    processURL(req, res)
  },
}
