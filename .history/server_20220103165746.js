var http = require('http')
const fs = require('fs')
const homePage = require('./pages/homePage')
const router = require('./router')

function start() {
  const server = http.createServer(function (req, res) {
    // 路由
    router.processImage(req, res)

    // 加载首页
    homePage.home(req, res)
  })

  server.listen(3001, 'localhost', () => {
    console.log(`Server running at http://localhost:3001/`)
  })
}

exports.start = start
