var http = require('http')
const router = require('./router')

function start() {
  const server = http.createServer(function (req, res) {
    // 处理路由信息
    router.processRoute(req, res)
  })

  server.listen(3001, 'localhost', () => {
    console.log(`Server running at http://localhost:3001/`)
  })
}

exports.start = start
