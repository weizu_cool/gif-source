const fs = require('fs')
const homePage = require('./pages/homePage')
const examplePages = require('./pages/examplePages')
const struils = require('./utils/stringUtil')

// 加载首页

const routes = {
  login: { page: examplePages.login, type: 'text/plain' },
  hello: { page: examplePages.hello, type: 'text/plain' },
  'index.html': { page: examplePages.login, type: 'text/html' },
}

function processImage(req, res) {
  //文件类型
  var type = req.url.substring(req.url.length - 4, req.url.length)
  //资源路径
  var fileName = __dirname + req.url
  //加载需要显示的图片资源
  if (type == '.jpg' || type == '.png') {
    res.writeHead(200, { 'Content-Type': 'text/' + type })
    res.end(fs.readFileSync(fileName))
  }
}

module.exports = {
  /**
   * 注册路由信息
   * @param {*} maps  link: function
   */
  registerRoute: function (req, res, maps) {
    var url = req.url
    console.log(url)
  },

  /**
   * 处理路由请求
   */
  processRoute: function (req, res) {
    // 处理图片
    processImage(req, res)
    // 其余链接
    var url = req.url
    if (
      struils.trim(url).length == 0 ||
      struils.trim(url) == '/' ||
      struils.trim(url) == '/index.html'
    ) {
    }
  },
}
