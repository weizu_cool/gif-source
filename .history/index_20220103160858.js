const http = require('http')
const fs = require('fs')

const server = http.createServer(function (req, res) {
  // 加载首页
  var filePath = 'index.html'
  fs.stat(filePath, function (error, data) {
    if (error) {
      res.writeHead(404)
      res.end('404 Not Found.')
    } else {
      res.writeHead(200)
      fs.createReadStream(filePath).pipe(res)
    }
  })
})

server.listen(3001, 'localhost', () => {
  console.log(`Server running at http://localhost:3001/`)
})
