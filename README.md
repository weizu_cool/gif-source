# GifSource

本地需要上传到`gitee`的图片放置在`public`目录中。
然后使用：

```javascript
node index.js
```

来执行`nodejs`服务端程序，打开链接`localhost:3001`然后刷新一下即可生成对应的`index.html`页面。

这里生成`html`采用最便捷的字符串拼接方式。其实更加规范一点应该是给服务器一个`json`文件，然后为这个文件创建一个前端可访问的接口，然后在`html`页面中根据接口的数据进行动态加载，那么就更加方便了。
